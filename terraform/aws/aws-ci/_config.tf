terraform {
  required_version = "~> 0.12"
}

#-- External providers -------------------------------------------------------
provider "aws" {
  version = "~> 2.0"
  region = var.aws_region
  assume_role {
    role_arn = "arn:aws:iam::${var.accounts[terraform.workspace]}:role/DevOps"
  }
}

data "aws_caller_identity" "aws_account" {}

provider "github" {
  token        = jsondecode(data.aws_secretsmanager_secret_version.github_token.secret_string)["GITHUB_TOKEN"]
  organization = var.github[terraform.workspace].Owner
}
