#-- Backend to store TF states -----------------------------------------------
terraform {
  backend "s3" {
    bucket = "<COMPANY>-terraform-states"
    key    = "aws-ci"
    region = "<REGION>"
  }
}
