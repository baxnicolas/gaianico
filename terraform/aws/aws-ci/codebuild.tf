resource "aws_iam_role" "codebuild_role" {
  name = "<COMPANY>-env-${terraform.workspace}-codebuild-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "codebuild.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "codebuild_role_policy" {
  role = aws_iam_role.codebuild_role.name

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Resource": [
        "*"
      ],
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ]
    },
    {
      "Effect":"Allow",
      "Action": [
        "s3:*"
      ],
      "Resource": [
        "${aws_s3_bucket.codepipeline_bucket.arn}",
        "${aws_s3_bucket.codepipeline_bucket.arn}/*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "ec2:CreateNetworkInterface",
        "ec2:DescribeDhcpOptions",
        "ec2:DescribeNetworkInterfaces",
        "ec2:DeleteNetworkInterface",
        "ec2:DescribeSubnets",
        "ec2:DescribeSecurityGroups",
        "ec2:DescribeVpcs"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "eks:DescribeCluster"
      ],
      "Resource": "*"
    },
    {
       "Effect":"Allow",
       "Action":[
          "ecr:GetAuthorizationToken"
       ],
       "Resource":"*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "ecr:GetAuthorizationToken",
        "ecr:InitiateLayerUpload",
        "ecr:UploadLayerPart",
        "ecr:CompleteLayerUpload",
        "ecr:BatchCheckLayerAvailability",
        "ecr:PutImage"
      ],
      "Resource": "arn:aws:ecr:${var.aws_region}:${data.aws_caller_identity.aws_account.account_id}:repository/${var.ecr_repo[terraform.workspace]}"
    },

    {
      "Effect":"Allow",
      "Action": [
        "s3:*"
      ],
      "Resource": [
        "arn:aws:s3:::${var.bucket_front_id[terraform.workspace]}",
        "arn:aws:s3:::${var.bucket_front_id[terraform.workspace]}/*"
      ]
    }
  ]
}
POLICY
}

data "template_file" "buildspec" {
  template = file("${path.module}/templates/buildspec.yaml.tpl")
  vars = {
    DOCKERFILE_PATH   = var.dockerfile_path[terraform.workspace]
    KUBE_VERSION      = var.kube_version[terraform.workspace]
    KUBE_CLUSTER      = "env-${terraform.workspace}-cluster"
    HELM_VERSION      = var.helm_version[terraform.workspace]
    HELM_RELEASE_NAME = var.helm_release_name[terraform.workspace]
    HELM_CHART_PATH   = var.helm_chart_path[terraform.workspace]
    HELM_VALUES_PATH  = var.helm_values_path[terraform.workspace]
    AWS_REGION        = var.aws_region
    AWS_ACCOUNT_ID    = data.aws_caller_identity.aws_account.account_id
    ECR_REPO          = var.ecr_repo[terraform.workspace]
  }
}

resource "aws_codebuild_project" "codebuild_project" {
  name           = "env-${terraform.workspace}-codebuild-project"
  description    = ""
  build_timeout  = "5"
  queued_timeout = "5"

  service_role  = aws_iam_role.codebuild_role.arn

  source {
    type            = "CODEPIPELINE"
    buildspec       = data.template_file.buildspec.rendered
  }

  artifacts {
    type = "CODEPIPELINE"
  }

  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    image                       = "aws/codebuild/standard:1.0"
    type                        = "LINUX_CONTAINER"
    image_pull_credentials_type = "CODEBUILD"
    privileged_mode             = true
  }
}


data "template_file" "buildspec_front" {
  template = file("${path.module}/templates/buildspec_front.yaml.tpl")
  vars = {
    DeployBucket = ${var.bucket_front_id[terraform.workspace]}
    front_dir = "app_react"
  }
}

resource "aws_codebuild_project" "codebuild_project_front" {
  name           = "env-${terraform.workspace}-codebuild-project-front"
  description    = ""
  build_timeout  = "5"
  queued_timeout = "5"

  service_role  = aws_iam_role.codebuild_role.arn

  source {
    type            = "CODEPIPELINE"
    buildspec       = data.template_file.buildspec_front.rendered
  }

  artifacts {
    type = "CODEPIPELINE"
  }

  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    type                        = "LINUX_CONTAINER"
    image                       = "aws/codebuild/nodejs:8.11.0"
    image_pull_credentials_type = "CODEBUILD"
  }
}
