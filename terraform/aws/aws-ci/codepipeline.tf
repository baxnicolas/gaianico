#-- IAM ----------------------------------------------------------

resource "aws_s3_bucket" "codepipeline_bucket" {
  bucket = "<COMPANY>-env-${terraform.workspace}-codepipeline-bucket"
  acl    = "private"
}

resource "aws_iam_role" "codepipeline_role" {
  name = "env-${terraform.workspace}-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "codepipeline.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "codepipeline_policy" {
  name = "env-${terraform.workspace}-codepipeline-policy"
  role = aws_iam_role.codepipeline_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect":"Allow",
      "Action": [
        "s3:GetObject",
        "s3:GetObjectVersion",
        "s3:GetBucketVersioning",
        "s3:PutObject"
      ],
      "Resource": [
        "${aws_s3_bucket.codepipeline_bucket.arn}",
        "${aws_s3_bucket.codepipeline_bucket.arn}/*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "codebuild:BatchGetBuilds",
        "codebuild:StartBuild"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}


#-- AWS and GitHub webhooks ----------------------------------------------------

locals {
  webhook_secret = "super-secret"
}

resource "aws_codepipeline_webhook" "codepipeline_webhook" {
  name            = "env-${terraform.workspace}-github-codepipeline_webhook"
  authentication  = "GITHUB_HMAC"
  target_action   = "Source"
  target_pipeline = aws_codepipeline.codepipeline.name

  authentication_configuration {
    secret_token = local.webhook_secret
  }

  filter {
    json_path    = "$.ref"
    # Note that the value of {Branch} is taken from
    #  - "aws_codepipeline/stages/action[name==Source]/configuration"
    match_equals = "refs/heads/{Branch}"
  }
}


resource "github_repository_webhook" "codepipeline_webhook" {
  repository = data.github_repository.repo_code.name

  configuration {
    url          = aws_codepipeline_webhook.codepipeline_webhook.url
    content_type = "json"
    insecure_ssl = true
    secret       = local.webhook_secret
  }

  events = ["push"]
}

data "github_repository" "repo_code" {
  full_name = "${var.github[terraform.workspace].Owner}/${var.github[terraform.workspace].Repo}"
}


#-- CodePipeline ---------------------------------------------------------------


# data "aws_kms_alias" "s3kmskey" {
#   name = "alias/myKmsKey"
# }

data "aws_secretsmanager_secret" "github_token" {
  name = "github-token"
}

data "aws_secretsmanager_secret_version" "github_token" {
  secret_id = data.aws_secretsmanager_secret.github_token.id
}

resource "aws_codepipeline" "codepipeline" {
  name     = "env-${terraform.workspace}-codepipeline"
  role_arn = aws_iam_role.codepipeline_role.arn

  artifact_store {
    location = aws_s3_bucket.codepipeline_bucket.bucket
    type     = "S3"

    # encryption_key {
    #   id   = data.aws_kms_alias.s3kmskey.arn
    #   type = "KMS"
    # }
  }

  stage {
    name = "Source"

    action {
      name             = "Source"
      category         = "Source"
      owner            = "ThirdParty"
      provider         = "GitHub"
      version          = "1"
      output_artifacts = ["source_output"]

      configuration = {
        OAuthToken           = jsondecode(data.aws_secretsmanager_secret_version.github_token.secret_string)["GITHUB_TOKEN"]
        Owner                = var.github[terraform.workspace].Owner
        Repo                 = var.github[terraform.workspace].Repo
        Branch               = var.github[terraform.workspace].Branch
        PollForSourceChanges = false
      }
    }
  }

  stage {
    name = "Build"

    action {
      name             = "Build"
      category         = "Build"
      owner            = "AWS"
      provider         = "CodeBuild"
      input_artifacts  = ["source_output"]
      output_artifacts = ["build_output"]
      version          = "1"

      configuration = {
        ProjectName = aws_codebuild_project.codebuild_project.name
      }
    }
  }

  stage {
    name = "BuildFront"

    action {
      name             = "BuildFront"
      category         = "Build"
      owner            = "AWS"
      provider         = "CodeBuild"
      input_artifacts  = ["source_output"]
      output_artifacts = ["build_front_output"]
      version          = "1"

      configuration = {
        ProjectName = aws_codebuild_project.codebuild_project_front.name
      }
    }
  }
}
