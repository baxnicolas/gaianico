#-- Bucket front --------------------------------------------------------------

variable "bucket_front_id" {
  type    = map
  default = {
    "app-staging" = "<FROND_ID>"
    "app-prod"    = "<FROND_ID>"
  }
}


#-- Github repo ---------------------------------------------------------------

variable "github" {
  type = map
  default = {
    # Careful: the owner is case sensitive !!!
    "app-staging" = {
                     Owner  = "<GITHUB_OWNER_ACCOUNT>"
                     Repo   = "<GITHUB_REPOSITORY>"
                     Branch = "staging"
    }
    "app-prod"    = {
                     Owner  = "<GITHUB_OWNER_ACCOUNT>"
                     Repo   = "<GITHUB_REPOSITORY>"
                     Branch = "master"
    }
  }
}


#-- CodeBuild -----------------------------------------------------------------

variable "kube_version" {
  type    = map
  default = {
    "app-staging" = "v2.17.0"
    "app-prod"    = "v2.17.0"
  }
}

variable "helm_version" {
  type    = map
  default = {
    "app-staging" = "v2.14.0"
    "app-prod"    = "v2.14.0"
  }
}

variable "dockerfile_path" {
  type    = map
  default = {
    "app-staging" = "docker/Dockerfile"
    "app-prod"    = "docker/Dockerfile"
  }
}

variable "ecr_repo" {
  type    = map
  default = {
    "app-staging" = "staging/api"
    "app-prod"    = "prod/api"
  }
}

variable "helm_release_name" {
  type    = map
  default = {
    "app-staging" = "api"
    "app-prod"    = "api"
  }
}

variable "helm_chart_path" {
  type    = map
  default = {
    "app-staging" = "kubernetes"
    "app-prod"    = "kubernetes"
  }
}

variable "helm_values_path" {
  type    = map
  default = {
    "app-staging" = "kubernetes/values.staging.yaml"
    "app-prod"    = "kubernetes/values.prod.yaml"
  }
}
