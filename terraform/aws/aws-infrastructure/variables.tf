#-- Users and groups ---------------------------------------------------------

# Users are defined inside a dict as following:
# <login> = <profile>
#
# Where <login> is user's email address and <profile> can be:
# - devops
# - developers

variable "users" {
  type    = map
  default = {
    "remip"     = "devops"
    "emmanuell" = "devops"
    "camils"    = "devops"
    "nicob"     = "devops"
    "fluentd"   = "cloudwatchlogger"
  }
}

# Groups map (<id> = <name>)
variable "groups" {
  type    = map
  default = {
    "developers"       = "Developers"
    "devops"           = "DevOps"
    "cloudwatchlogger" = "CloudWatchLogger"
  }
}

# Groups map (<id> = <name>)
variable "groups_aws" {
  type    = map
  default = {
    "developers"       = "arn:aws:iam::aws:policy/ReadOnlyAccess"
  }
}

#-- Networking ---------------------------------------------------------------
variable "vpc_tools_cidr" {
  default = "172.30.0.0/20"
}

variable "vpc_tools_subnet_size" {
  default = "4"              # 20+4 => /24 subnet
}

variable "inbound_whitelist_ips" {
  type    = list
  default = [
    "0.0.0.0/0"        # CHANGE THAT VARIABLE
  ]
}

#-- ECR repositories ---------------------------------------------------------
variable "ecr_registries" {
  type    = list
  default = [
    "team-infra/helm-deploy",
    "staging/api",
    "prod/api"
  ]
}
