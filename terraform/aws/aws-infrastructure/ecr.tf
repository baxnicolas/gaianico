#-- Manage ECR repositories --------------------------------------------------
resource "aws_ecr_repository" "ecr_registry" {
  for_each = toset(var.ecr_registries)
  name     = each.key
  tags     = var.resource_tags
}

resource "aws_ecr_lifecycle_policy" "ecr_registry" {
  for_each   = toset(var.ecr_registries)
  repository = aws_ecr_repository.ecr_registry[each.key].name
  policy     = <<EOP
{
  "rules": [
    {
      "rulePriority": 1,
      "description": "Remove untagged images after 30 days",
      "selection": {
        "tagStatus": "untagged",
        "countType": "sinceImagePushed",
        "countUnit": "days",
        "countNumber": 30
      },
      "action": {
        "type": "expire"
      }
    }
  ]
}
EOP
}
