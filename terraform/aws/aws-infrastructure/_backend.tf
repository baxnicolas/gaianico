#-- Backend to store TF states -----------------------------------------------
terraform {
  backend "s3" {
    bucket = "padok-terraform-states"
    key    = "awsinfrastructure"
    region = "us-east-1"
  }
}
