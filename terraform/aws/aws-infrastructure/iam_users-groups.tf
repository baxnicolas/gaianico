#-- Global account policy ----------------------------------------------------
resource "aws_iam_account_password_policy" "policy" {
  minimum_password_length        = 8
  require_lowercase_characters   = true
  require_numbers                = true
  require_uppercase_characters   = true
  require_symbols                = true
  allow_users_to_change_password = true
}

#-- IAM accounts -------------------------------------------------------------
resource "aws_iam_user" "user" {
  for_each      = var.users

  name          = each.key
  force_destroy = true
  tags          = var.resource_tags
}

resource "aws_iam_group" "groups" {
  for_each = var.groups

  name     = each.value
}

resource "aws_iam_group_membership" "groups" {
  for_each   = var.groups
  depends_on = ["aws_iam_user.user"]

  name     = each.key
  group    = aws_iam_group.groups[each.key].name
  users    = [for k, v in var.users : k if v == each.key]
}

resource "aws_iam_group_policy" "groups" {
  for_each = var.groups
  depends_on = ["aws_iam_user.user", "aws_iam_group.groups"]

  name   = each.key
  group  = aws_iam_group.groups[each.key].name
  policy = file("${path.module}/files/iam_group_policy_${each.key}.json")
}

resource "aws_iam_group_policy_attachment" "groups_attachements" {
  for_each = var.groups_aws
  depends_on = ["aws_iam_group.groups"]

  group      = each.key
  policy_arn = each.value
}

#-- Manual group output to share accounts with 'aws-environments' ------------
output "users_devops" {
  value = concat([for k, v in var.users : k if v == "devops"])
}

output "users_developers" {
  value = concat([for k, v in var.users : k if v == "developers"])
}

#-- Assume role policy for DevOps --------------------------------------------
resource "aws_iam_policy" "assume-role-devops" {
  name        = "Assume-Role-DevOps"
  description = "Allow assuming 'DevOps' role on PROD account"
  policy      = templatefile("${path.module}/files/environment_assume_policy.tpl", { role_name = "DevOps", accounts = values(var.accounts) }) 
}

resource "aws_iam_group_policy_attachment" "assume-role-devops" {
  group      = aws_iam_group.groups["devops"].name
  policy_arn = aws_iam_policy.assume-role-devops.arn
}

#-- Assume role policy for Developers --------------------------------------------
resource "aws_iam_policy" "assume-role-developers" {
  name        = "Assume-Role-Developer"
  description = "Allow assuming 'Developer' role on all environment accounts"
  policy      = templatefile("${path.module}/files/environment_assume_policy.tpl", { role_name = "Developers", accounts = values(var.accounts) }) 
}

resource "aws_iam_group_policy_attachment" "assume-role-developers" {
  group      = aws_iam_group.groups["developers"].name
  policy_arn = aws_iam_policy.assume-role-developers.arn
}
