terraform {
  required_version = "~> 0.12.6"
}

#-- External providers -------------------------------------------------------
provider "aws" {
  version = "~> 2.0"
  region  = var.aws_region
}

provider "tls" {
  version = "~> 2.0"
}
