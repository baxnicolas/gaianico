resource "aws_db_subnet_group" "env_db_subnet" {
  name       = "env-${terraform.workspace}-db-subnet_group"
  subnet_ids = aws_subnet.env_private.*.id

#  tags = TODO
}

resource "aws_db_parameter_group" "env_db_params" {
  name   = "env-${terraform.workspace}-db-params-group"
  family = "postgres9.6"

#  parameter {
#    name  = "skip_final_snapshot"
#    value = true
#  }

#tags = TODO
}


resource "random_password" "env_db_password" {
  length           = 128
  override_special = "#,"
}

resource "aws_secretsmanager_secret" "env_db_secret" {
  name = "env-${terraform.workspace}-pgsql-db"
  recovery_window_in_days = 0
}

resource "aws_secretsmanager_secret_version" "env_db_secret" {
  secret_id = aws_secretsmanager_secret.env_db_secret.id
  secret_string = jsonencode({
                   db_password = random_password.env_db_password.result
                  })
}

resource "aws_db_instance" "env_db" {
  identifier           = "env-${terraform.workspace}-db"
  engine               = "postgres"
  engine_version       = var.engine_version[terraform.workspace]
  storage_type         = var.storage_type[terraform.workspace]
  instance_class       = var.instance_class[terraform.workspace]

  allocated_storage     = var.allocated_storage[terraform.workspace]
  max_allocated_storage = var.max_allocated_storage[terraform.workspace]

  name                 = "env${replace(title(terraform.workspace),"-","")}DB"
  username             = var.username[terraform.workspace]
  password             = random_password.env_db_password.result

  parameter_group_name = aws_db_parameter_group.env_db_params.name

  backup_retention_period = "7"
  backup_window           = "03:50-04:20"

  availability_zone = "${var.aws_region}${var.aws_region_az[0]}"
  db_subnet_group_name = aws_db_subnet_group.env_db_subnet.name

  vpc_security_group_ids = [aws_security_group.env_cluster_nodes.id]
  publicly_accessible = false

  skip_final_snapshot = true

#tags = TODO
}
