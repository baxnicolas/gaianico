#-- Remove states ------------------------------------------------------------
data "terraform_remote_state" "aws-infrastructure" {
  backend = "s3"
  config  = {
    bucket = "padok-terraform-states"
    key    = "awsinfrastructure"
    region = "us-east-1"
  }
}
