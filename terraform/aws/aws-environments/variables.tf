#-- IAM -----------------------------------------------------------------------
variable "roles" {
  type    = map
  default = {
    "developers" = "Developers"
  }
}

#-- Networking ---------------------------------------------------------------

variable "vpc_cidr" {
  type    = map
  default = {
    "app-staging" = "172.30.16.0/20"
    "app-prod"    = "172.30.32.0/20"
  }
}

variable "vpc_subnet_size" {
  type    = map
  default = {
    "app-staging" = "4"     # 20+4 => /24 subnet
    "app-prod"    = "4"
  }
}


#-- DNS ----------------------------------------------------------------------

variable "root_domain" {
  default = "4padok.com"
}

variable "env_subdomain" {
  default = "env.4padok.com"
}


#-- Cluster nodes ------------------------------------------------------------

variable "nodes_instance_type" {
  type    = map
  default = {
    "app-staging" = "m4.large"
    "app-prod"    = "m4.large"
  }
}

variable "nodes_instance_disk" {
  type    = map
  default = {
    "app-staging" = "20"
    "app-prod"    = "20"
  }
}

variable "nodes_instance_min" {
  type    = map
  default = {
    "app-staging" = "0"
    "app-prod"    = "0"
  }
}

variable "nodes_instance_max" {
  type    = map
  default = {
    "app-staging" = "3"
    "app-prod"    = "3"
  }
}

variable "nodes_instance_desired" {
  type    = map
  default = {
    "app-staging" = "0"
    "app-prod"    = "0"
  }
}

locals {
  cluster_nodes_userdata = <<BOOTSTRAP
#!/bin/bash
set -o xtrace
/etc/eks/bootstrap.sh --apiserver-endpoint '${aws_eks_cluster.env_cluster.endpoint}' --b64-cluster-ca '${aws_eks_cluster.env_cluster.certificate_authority.0.data}' 'env-${terraform.workspace}-cluster'
BOOTSTRAP
}

variable "kubernetes_additional_users" {
  type    = map
  default = {
    "app-staging" = { "padok-env-app-staging-codebuild-role" = "role" }
    "app-prod" = { "padok-env-app-prod-codebuild-role" = "role" }
  }
}


#-- Database -----------------------------------------------------------------

variable "engine_version" {
  type    = map
  default = {
    "app-staging" = "9.6"
    "app-prod"    = "9.6"
  }
}

variable "storage_type" {
  type    = map
  default = {
    "app-staging" = "gp2"
    "app-prod"    = "gp2"
  }
}

variable "instance_class" {
  type    = map
  default = {
    "app-staging" = "db.t2.micro"
    "app-prod"    = "db.t2.micro"
  }
}

variable "allocated_storage" {
  type    = map
  default = {
    "app-staging" = "10"
    "app-prod"    = "10"
  }
}

variable "max_allocated_storage" {
  type    = map
  default = {
    "app-staging" = "30"
    "app-prod"    = "30"
  }
}

variable "username" {
  type    = map
  default = {
    "app-staging" = "foo"
    "app-prod"    = "foo"
  }
}

#-- Bucket front --------------------------------------------------------------

variable "front_index_document" {
  type    = map
  default = {
    "app-staging" = "index.html"
    "app-prod"    = "index.html"
  }
}
