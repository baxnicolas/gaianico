resource "kubernetes_service" "database" {
  metadata {
    name = "database"
    namespace = "default"
  }
  spec {
    external_name = aws_db_instance.env_db.address
    type = "ExternalName"
    port {
      port = aws_db_instance.env_db.port
    }
  }
}
