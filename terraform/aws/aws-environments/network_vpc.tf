#-- VPC ----------------------------------------------------------------------
resource "aws_vpc" "env_vpc" {
  cidr_block           = var.vpc_cidr[terraform.workspace]
  enable_dns_hostnames = "true"
  tags                 = merge(
                               var.resource_tags,
                               map("Name", "ENV ${title(terraform.workspace)}"),
                               map("Environment", "${title(terraform.workspace)}"),
                               map("kubernetes.io/cluster/env-${terraform.workspace}-cluster","shared")
                              )
}

#-- Subnets ------------------------------------------------------------------
resource "aws_subnet" "env_public" {
  count             = length(var.aws_region_az)
  vpc_id            = aws_vpc.env_vpc.id
  cidr_block        = cidrsubnet(var.vpc_cidr[terraform.workspace], var.vpc_subnet_size[terraform.workspace], count.index)
  availability_zone = "${var.aws_region}${var.aws_region_az[count.index]}"
  map_public_ip_on_launch = true
  tags              = merge(
                            var.resource_tags,
                            map("Name", "ENV ${title(terraform.workspace)} Public (zone-${var.aws_region_az[count.index]})"),
                            map("Environment", "${title(terraform.workspace)}"),
                            map("kubernetes.io/cluster/env-${terraform.workspace}-cluster","shared")
                           )
}

resource "aws_subnet" "env_private" {
  count             = length(var.aws_region_az)
  vpc_id            = aws_vpc.env_vpc.id
  cidr_block        = cidrsubnet(var.vpc_cidr[terraform.workspace], var.vpc_subnet_size[terraform.workspace], length(var.aws_region_az) + count.index)
  availability_zone = "${var.aws_region}${var.aws_region_az[count.index]}"
  tags              = merge(
                            var.resource_tags,
                            map("Name", "ENV ${title(terraform.workspace)} Private (zone-${var.aws_region_az[count.index]})"),
                            map("Environment", "${title(terraform.workspace)}"),
                            map("kubernetes.io/cluster/env-${terraform.workspace}-cluster","shared")
                           )
}

#-- NAT Gateway -------------------------------------------------------------
resource "aws_eip" "env_public" {
  count    = length(var.aws_region_az)
  vpc      = true
  tags     = merge(
                   var.resource_tags,
                   map("Name", "ENV ${title(terraform.workspace)} gateway (zone-${var.aws_region_az[count.index]})"),
                   map("Environment", "${title(terraform.workspace)}")
                  )
}

resource "aws_nat_gateway" "env_public" {
  count             = length(var.aws_region_az)
  subnet_id         = aws_subnet.env_public.*.id[count.index]
  allocation_id     = aws_eip.env_public.*.id[count.index]
  tags              = merge(
                            var.resource_tags,
                            map("Name", "ENV ${title(terraform.workspace)} (zone-${var.aws_region_az[count.index]})"),
                            map("Environment", "${title(terraform.workspace)}")
                           )
}

#-- Route tables -------------------------------------------------------------
resource "aws_route_table" "env_public" {
  vpc_id = aws_vpc.env_vpc.id
  tags   = merge(
                 var.resource_tags,
                 map("Name", "ENV ${title(terraform.workspace)} route (VPC public)"),
                 map("Environment", "${title(terraform.workspace)}"),
                )

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.env_gateway.id
  }
}

resource "aws_route_table" "env_private" {
  count  = length(var.aws_region_az)
  vpc_id = aws_vpc.env_vpc.id
  tags   = merge(
                 var.resource_tags,
                 map("Name", "ENV ${title(terraform.workspace)} route (zone-${var.aws_region_az[count.index]} private)"),
                 map("Environment", "${title(terraform.workspace)}"),
                )

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.env_public.*.id[count.index]
  }
}


resource "aws_route_table_association" "env_public" {
  count          = length(var.aws_region_az)
  subnet_id      = aws_subnet.env_public.*.id[count.index]
  route_table_id = aws_route_table.env_public.id
}

resource "aws_route_table_association" "env_private" {
  count          = length(var.aws_region_az)
  subnet_id      = aws_subnet.env_private.*.id[count.index]
  route_table_id = aws_route_table.env_private.*.id[count.index]
}


#-- Internet gateway ---------------------------------------------------------
resource "aws_internet_gateway" "env_gateway" {
  vpc_id = aws_vpc.env_vpc.id
  tags   = merge(
                 var.resource_tags,
                 map("Name", "ENV ${title(terraform.workspace)} gateway"),
                 map("Environment", "${title(terraform.workspace)}")
                )
}
