#-- Prometheus-operator -------------------------------------------------

resource "random_password" "grafana_password" {
  length           = 128
  override_special = ""
}

resource "aws_secretsmanager_secret" "grafana_secret" {
  name = "env-${terraform.workspace}-grafana"
  recovery_window_in_days = 0
}

resource "aws_secretsmanager_secret_version" "grafana_secret" {
  secret_id = aws_secretsmanager_secret.grafana_secret.id
  secret_string = jsonencode({
                   admin_password = random_password.grafana_password.result
                  })
}

resource "helm_release" "prometheus-operator" {
    name      = "prometheus-operator"
    chart     = "stable/prometheus-operator"
    namespace = "kube-system"

    set_string {
        name = "grafana.adminPassword"
        value = random_password.grafana_password.result
    }

    set_string {
        name = "grafana.persistence.enabled"
        value = true
    }

    set_string {
        name = "grafana.ingress.enabled"
        value = true
    }

    set_string {
        name = "grafana.ingress.hosts"
        value = "{grafana.${terraform.workspace}.${var.env_subdomain}}"
    }
}
