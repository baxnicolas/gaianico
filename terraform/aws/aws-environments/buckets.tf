#-- S3 Bucket for front data ---------------------------------------------

data "template_file" "policy_bucket_front" {
  template = file("templates/bucket_policy_front.json.tpl")
  vars = {
    bucket_name = "${terraform.workspace}.${var.env_subdomain}"
  }
}

resource "aws_s3_bucket" "bucket_front" {
  bucket = "${terraform.workspace}.${var.env_subdomain}"
  region = var.aws_region
  acl    = "public-read"
  policy = data.template_file.policy_bucket_front.rendered
#  tags   = TODO

  website {
    index_document = var.front_index_document[terraform.workspace]
  }

#  cors {
#    allowed_headers = ["*"]
#    allowed_methods = ["PUT", "POST"]
#    allowed_origins = ["https://s3-website-test.hashicorp.com"]
#    expose_headers  = ["ETag"]
#    max_age_seconds = 3000
#  }
}

#-- S3 Bucket for other data ---------------------------------------------

data "template_file" "policy_bucket_data" {
  template = file("templates/bucket_policy_data.json.tpl")
  vars = {
    workspace = terraform.workspace
  }
}

resource "aws_s3_bucket" "bucket_data" {
  bucket = "padok-env-${terraform.workspace}-data"
  region = var.aws_region
  acl    = "public-read"
  policy = data.template_file.policy_bucket_data.rendered

#  tags   = TODO
}
