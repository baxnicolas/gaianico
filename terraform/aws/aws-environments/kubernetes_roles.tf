#-- Service account for the cluster ------------------------------------------
resource "aws_iam_role" "cluster" {
  name               = "env-${terraform.workspace}-cluster-role"
  description        = "Enables Kubernetes ${terraform.workspace} cluster to interract with other services"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF

  tags = merge(
               var.resource_tags,
               map("Environment", "${title(terraform.workspace)}")
              )
}

resource "aws_iam_role_policy_attachment" "cluster_policy_AmazonEKSClusterPolicy" {
  role       = aws_iam_role.cluster.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
}

resource "aws_iam_role_policy_attachment" "cluster_policy_AmazonEKSServicePolicy" {
  role       = aws_iam_role.cluster.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
}

#-- Service account for the nodes --------------------------------------------
resource "aws_iam_role" "cluster_nodes" {
  name               = "env-${terraform.workspace}-nodes-role"
  description        = "Enables Kubernetes ${terraform.workspace} cluster to deploy client nodes"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF

  tags = merge(
               var.resource_tags,
               map("Environment", "${title(terraform.workspace)}")
              )
}

resource "aws_iam_policy" "cluster_nodes_autoscaler" {
  name        = "env-${terraform.workspace}-nodes-autoscale"
  description = "Allow nodes to interrogate ASG in order enable autoscaling"
  policy      = <<EOFSCALER
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "autoscaling:DescribeAutoScalingGroups",
        "autoscaling:DescribeAutoScalingInstances",
        "autoscaling:DescribeLaunchConfigurations",
        "autoscaling:DescribeTags"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "autoscaling:SetDesiredCapacity",
        "autoscaling:TerminateInstanceInAutoScalingGroup"
      ],
      "Resource": "${aws_autoscaling_group.env_cluster_nodes.arn}"
    }
  ]
}
EOFSCALER
}

resource "aws_iam_policy" "route53_auto_managed" {
  name        = "env-${terraform.workspace}-route53-auto-managed"
  description = "Allow nodes to manage their workspace-specific Route53 DNS zone"
  policy      = <<EOFSCALER
{
 "Version": "2012-10-17",
 "Statement": [
   {
     "Effect": "Allow",
     "Action": [
       "route53:ChangeResourceRecordSets"
     ],
     "Resource": [
       "arn:aws:route53:::hostedzone/${aws_route53_zone.env_padok_co.zone_id}"
     ]
   },
   {
     "Effect": "Allow",
     "Action": [
       "route53:ListHostedZones",
       "route53:ListResourceRecordSets"
     ],
     "Resource": [
       "*"
     ]
   }
 ]
}
EOFSCALER
}

resource "aws_iam_role_policy_attachment" "nodes_policy_route53_auto_managed" {
  role       = aws_iam_role.cluster_nodes.name
  policy_arn = aws_iam_policy.route53_auto_managed.arn
}

resource "aws_iam_role_policy_attachment" "nodes_policy_autoscaler" {
  role       = aws_iam_role.cluster_nodes.name
  policy_arn = aws_iam_policy.cluster_nodes_autoscaler.arn
}

resource "aws_iam_role_policy_attachment" "nodes_policy_AmazonEKSWorkerNodePolicy" {
  role       = aws_iam_role.cluster_nodes.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
}

resource "aws_iam_role_policy_attachment" "nodes_policy_AmazonEKS_CNI_Policy" {
  role       = aws_iam_role.cluster_nodes.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
}

resource "aws_iam_role_policy_attachment" "nodes_policy_AmazonEC2ContainerRegistryPowerUser" {
  role       = aws_iam_role.cluster_nodes.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

resource "aws_iam_role_policy_attachment" "nodes_policy_SecretsManagerReadWrite" {
  role       = aws_iam_role.cluster_nodes.name
  policy_arn = "arn:aws:iam::aws:policy/SecretsManagerReadWrite"
}

resource "aws_iam_instance_profile" "env_cluster_nodes" {
  name = "env-${terraform.workspace}-cluster-nodes"
  role = aws_iam_role.cluster_nodes.name
}
