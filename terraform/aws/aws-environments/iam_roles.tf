resource "aws_iam_role" "roles" {
  for_each = var.roles

  name = each.value
  assume_role_policy = templatefile("${path.module}/templates/iam_role_trust_relationship_policy.json.tpl", { trusted_account: var.infra_account })

}

resource "aws_iam_role_policy" "role_policies" {
  for_each = var.roles

  name    =  each.key
  role    =  aws_iam_role.roles[each.key].name
  policy  =  file("${path.module}/files/iam_role_policy_${each.key}.json")
}
