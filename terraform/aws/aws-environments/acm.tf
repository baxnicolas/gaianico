resource "aws_acm_certificate" "wildcard_env_padok_co" {
  domain_name       = "*.${terraform.workspace}.${var.env_subdomain}"
  validation_method = "DNS"
}

###
# The following will only work when you have a DNS domain you control
###

resource "aws_acm_certificate_validation" "wildcard_env_padok_co" {
  certificate_arn         = aws_acm_certificate.wildcard_env_padok_co.arn
  validation_record_fqdns = [aws_route53_record.wildcard_env_padok_co_validation.fqdn]
}

resource "aws_route53_record" "wildcard_env_padok_co_validation" {
  name    = aws_acm_certificate.wildcard_env_padok_co.domain_validation_options.0.resource_record_name
  type    = aws_acm_certificate.wildcard_env_padok_co.domain_validation_options.0.resource_record_type
  zone_id = aws_route53_zone.env_padok_co.zone_id
  records = [aws_acm_certificate.wildcard_env_padok_co.domain_validation_options.0.resource_record_value]
  ttl     = 60
}


#-- CloudFront dedicated ACM certificate ---------------------------------------------------------------

resource "aws_acm_certificate" "wildcard_cloudfront" {
  provider = aws.us

  domain_name       = "*.${terraform.workspace}.${var.env_subdomain}"
  validation_method = "DNS"
}
