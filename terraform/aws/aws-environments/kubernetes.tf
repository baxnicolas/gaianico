#-- Kubernetes cluster -------------------------------------------------------
resource "aws_eks_cluster" "env_cluster" {
  name     = "env-${terraform.workspace}-cluster"
  role_arn = aws_iam_role.cluster.arn
  version  = "1.14"

  vpc_config {
    endpoint_private_access = false
    endpoint_public_access  = true
    security_group_ids      = [aws_security_group.env_cluster.id]
    subnet_ids              = aws_subnet.env_public.*.id
  }

  depends_on = [
    aws_iam_role_policy_attachment.cluster_policy_AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.cluster_policy_AmazonEKSServicePolicy
  ]
}

# ConfigMap to allow nodes to join cluster via their IAM role
# and IAM accounts with their specified profile.
resource "kubernetes_config_map" "nodes_auth" {
  metadata {
    name      = "aws-auth"
    namespace = "kube-system"
  }

  data = {
    "mapRoles" = "- rolearn: ${aws_iam_role.cluster_nodes.arn}\n  username: system:node:{{EC2PrivateDNSName}}\n  groups:\n    - system:bootstrappers\n    - system:nodes\n"
    "mapUsers" = "${join("",formatlist("- userarn: arn:aws:iam::${data.aws_caller_identity.aws_account.account_id}:%s/%s\n  username: %s\n  groups:\n    - system:masters\n", [for k, v in var.kubernetes_additional_users[terraform.workspace] : v], [for k, v in var.kubernetes_additional_users[terraform.workspace] : k], [for k, v in var.kubernetes_additional_users[terraform.workspace] : k]))}\n${join("",formatlist("- userarn: arn:aws:iam::${data.aws_caller_identity.aws_account.account_id}:user/%s\n  username: %s\n  groups:\n    - system:masters\n", [for user in data.terraform_remote_state.aws-infrastructure.outputs.users_devops : user], [for user in data.terraform_remote_state.aws-infrastructure.outputs.users_devops : user]))}"
  }
}
