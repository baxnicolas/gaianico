#-- Nodes AMI ----------------------------------------------------------------
data "aws_ami" "cluster_nodes_ami" {
  filter {
    name   = "name"
    values = ["amazon-eks-node-${aws_eks_cluster.env_cluster.version}-v*"]
  }

  most_recent = true
  owners      = ["602401143452"] # Amazon EKS AMI Account ID
}

#-- Autoscaling group --------------------------------------------------------
resource "aws_launch_configuration" "env_cluster_nodes" {
  iam_instance_profile        = aws_iam_instance_profile.env_cluster_nodes.name
  image_id                    = data.aws_ami.cluster_nodes_ami.id
  instance_type               = var.nodes_instance_type[terraform.workspace]
  name_prefix                 = "env_${terraform.workspace}_nodes_"
  security_groups             = [aws_security_group.env_cluster_nodes.id]
  user_data_base64            = base64encode(local.cluster_nodes_userdata)
  associate_public_ip_address = false
  enable_monitoring           = true
  ebs_optimized               = true
  key_name                    = aws_key_pair.cluster_nodes_key.key_name

  lifecycle {
    create_before_destroy = true
  }

  root_block_device {
    delete_on_termination = true
    volume_size           = var.nodes_instance_disk[terraform.workspace]
    volume_type           = "gp2"
  }
}

resource "aws_placement_group" "cluster_nodes" {
  name     = "env_${terraform.workspace}_cluster_nodes_placement"
  strategy = "partition"
}

resource "aws_autoscaling_group" "env_cluster_nodes" {
  name                 = "env-${terraform.workspace}-cluster-nodes"
  launch_configuration = aws_launch_configuration.env_cluster_nodes.id
  vpc_zone_identifier  = aws_subnet.env_private.*.id
  placement_group      = aws_placement_group.cluster_nodes.id
  min_size             = var.nodes_instance_min[terraform.workspace]
  max_size             = var.nodes_instance_max[terraform.workspace]
  desired_capacity     = var.nodes_instance_desired[terraform.workspace]
  health_check_type    = "EC2"
  force_delete         = true

  # lifecycle {
  #   ignore_changes = [desired_capacity]
  # }

  tag {
    key                 = "kubernetes.io/cluster/env-${terraform.workspace}-cluster"
    value               = "owned"
    propagate_at_launch = true
  }

  tag {
    key                 = "Environment"
    value               = title(terraform.workspace)
    propagate_at_launch = true
  }

  tag {
    key                 = "Name"
    value               = "ENV ${title(terraform.workspace)} cluster node"
    propagate_at_launch = true
  }

  tag {
    key                 = "k8s.io/cluster-autoscaler/enabled"
    value               = "true"
    propagate_at_launch = false
  }

  tag {
    key                 = "k8s.io/cluster/env-${terraform.workspace}-cluster"
    value               = "enabled"
    propagate_at_launch = false
  }
}
