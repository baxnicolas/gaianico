terraform {
  required_version = "~> 0.12"
}

#-- External providers -------------------------------------------------------
provider "aws" {
  version = "~> 2.0"
  region = var.aws_region
  assume_role {
    role_arn = "arn:aws:iam::${var.accounts[terraform.workspace]}:role/DevOps"
  }
}

#-- Provider in us-east-1 (CloudFront certificate only) ----------------------
provider "aws" {
  alias = "us"
  version = "~> 2.0"
  region = "us-east-1"
  assume_role {
    role_arn = "arn:aws:iam::${var.accounts[terraform.workspace]}:role/DevOps"
  }
}

provider "tls" {
  version = "~> 2.0"
}

data "aws_eks_cluster_auth" "cluster_auth" {
  name = "env-${terraform.workspace}-cluster"
}

data "aws_caller_identity" "aws_account" {}

provider "kubernetes" {
  load_config_file       = false
  host                   = aws_eks_cluster.env_cluster.endpoint
  cluster_ca_certificate = base64decode(aws_eks_cluster.env_cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster_auth.token
  version                = "~> 1.8"
}

provider "helm" {
  tiller_image                    = "gcr.io/kubernetes-helm/tiller:v2.14.0"
  install_tiller                  = true
  service_account                 = kubernetes_service_account.tiller.metadata.0.name
  automount_service_account_token = true
  namespace                       = kubernetes_service_account.tiller.metadata.0.namespace
  max_history                     = "10"
  version                         = "~> 0.10"

  kubernetes {
    load_config_file       = false
    host                   = aws_eks_cluster.env_cluster.endpoint
    cluster_ca_certificate = base64decode(aws_eks_cluster.env_cluster.certificate_authority.0.data)
    token                  = data.aws_eks_cluster_auth.cluster_auth.token
  }
}
