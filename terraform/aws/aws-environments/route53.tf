#-- Create DNS zone for current env ------------------------------------------
resource "aws_route53_zone" "env_padok_co" {
  name          = "${terraform.workspace}.${var.env_subdomain}"
  comment       = "Zone for '${terraform.workspace}' environment"
  force_destroy = "true"
  tags          = merge(
                        var.resource_tags,
                        map("Environment", "${title(terraform.workspace)}"),
                       )
}

#data "aws_route53_zone" "<COMPANY>_co" {
#  name     = var.root_domain
#}
#
#resource "aws_route53_record" "env_<COMPANY>_co" {
#  zone_id  = data.aws_route53_zone.<COMPANY>_co.zone_id
#  name     = aws_route53_zone.env_<COMPANY>_co.name
#  type     = "NS"
#  ttl      = "300"
#  records  = aws_route53_zone.env_<COMPANY>_co.name_servers
#}


#-- Add aliases for CloudFront -----------------------------------------------

resource "aws_route53_record" "env_padok_co_front" {
  zone_id  = aws_route53_zone.env_padok_co.zone_id
  name     = "www.${terraform.workspace}.${var.env_subdomain}"
  type     = "A"

  alias {
    name                   = aws_cloudfront_distribution.s3_distribution.domain_name
    zone_id                = aws_cloudfront_distribution.s3_distribution.hosted_zone_id
    evaluate_target_health = false
  }
}
