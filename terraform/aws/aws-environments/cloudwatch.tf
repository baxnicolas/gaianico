resource "aws_cloudwatch_log_group" "fluentd_log_group" {
  name = "/aws/eks/env-${terraform.workspace}-logs"

  retention_in_days = 30

#  tags = TODO
}
