#-- Deploy Weave network plugin ----------------------------------------------
#
# This part is extracted from version 2.5.2:
# https://github.com/weaveworks/weave/releases/tag/latest_release

resource "kubernetes_service_account" "weave" {
  metadata {
    name      = "weave-net"
    namespace = "kube-system"
    labels    = {
      name = "weave-net"
    }
  }
}

resource "kubernetes_cluster_role" "weave" {
  metadata {
    name   = kubernetes_service_account.weave.metadata.0.name
    labels = {
      name = kubernetes_service_account.weave.metadata.0.name
    }
  }

  rule {
    api_groups = [""]
    resources  = ["pods", "namespaces", "nodes"]
    verbs      = ["get", "list", "watch"]
  }

  rule {
    api_groups = ["networking.k8s.io"]
    resources  = ["networkpolicies"]
    verbs      = ["get", "list", "watch"]
  }

  rule {
    api_groups = [""]
    resources  = ["nodes/status"]
    verbs      = ["update", "patch"]
  }
}

resource "kubernetes_cluster_role_binding" "weave" {
  metadata {
    name = kubernetes_service_account.weave.metadata.0.name
    labels = {
      name = kubernetes_service_account.weave.metadata.0.name
    }
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_service_account.weave.metadata.0.name
  }

  subject {
    kind      = "ServiceAccount"
    namespace = "kube-system"
    name      = kubernetes_service_account.weave.metadata.0.name
  }
}

resource "kubernetes_role" "weave" {
  metadata {
    name      = kubernetes_service_account.weave.metadata.0.name
    namespace = "kube-system"
    labels    = {
      name = kubernetes_service_account.weave.metadata.0.name
    }
  }

  rule {
    api_groups     = [""]
    resources      = ["configmaps"]
    verbs          = ["create"]
  }

  rule {
    api_groups     = [""]
    resource_names = [kubernetes_service_account.weave.metadata.0.name]
    resources      = ["configmaps"]
    verbs          = ["get","update"]
  }

}

resource "kubernetes_role_binding" "weave" {
  metadata {
    name = kubernetes_service_account.weave.metadata.0.name
    namespace = "kube-system"
    labels = {
      name = kubernetes_service_account.weave.metadata.0.name
    }
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = kubernetes_service_account.weave.metadata.0.name
  }

  subject {
    kind      = "ServiceAccount"
    namespace = "kube-system"
    name      = kubernetes_service_account.weave.metadata.0.name
  }
}

resource "kubernetes_daemonset" "weave" {
  metadata {
    name      = kubernetes_service_account.weave.metadata.0.name
    namespace = "kube-system"
    labels    = {
      name = kubernetes_service_account.weave.metadata.0.name
    }
  }

  spec {
    selector {
      match_labels = {
        name = kubernetes_service_account.weave.metadata.0.name
      }
    }

    template {
      metadata {
        labels = {
          name = kubernetes_service_account.weave.metadata.0.name
        }
      }

      spec {
        host_network                    = true
        host_pid                        = true
        restart_policy                  = "Always"
        service_account_name            = kubernetes_service_account.weave.metadata.0.name
        automount_service_account_token = true
        security_context {
          se_linux_options {}
        }

        toleration {
          key      = "dedicated"
          operator = "Equal"
          value    = "master"
          effect   = "NoSchedule"
        }
################### SPECIFIC TOLERATIONS #####################
### Required for pods to be scheduled before Node is ready ###
        toleration {
          operator = "Exists"
          effect   = "NoSchedule"
        }

        toleration {
          operator = "Exists"
          effect   = "NoExecute"
        }
##############################################################
        container {
          name    = "weave"
          command = ["/home/weave/launch.sh"]
          image   = "docker.io/weaveworks/weave-kube:2.6.0"

          env {
            name = "HOSTNAME"
            value_from {
              field_ref {
                api_version = "v1"
                field_path  = "spec.nodeName"
              }
            }
          }

          readiness_probe {
            http_get {
              host = "127.0.0.1"
              port = 6784
              path = "/status"
            }
          }

          resources {
            requests {
              cpu = "10m"
            }
          }

          security_context {
            privileged = true
          }

          volume_mount {
            name       = "weavedb"
            mount_path = "/weavedb"
          }

          volume_mount {
            name       = "cni-bin"
            mount_path = "/host/opt"
          }

          volume_mount {
            name       = "cni-bin2"
            mount_path = "/host/home"
          }

          volume_mount {
            name       = "cni-conf"
            mount_path = "/host/etc"
          }

          volume_mount {
            name       = "dbus"
            mount_path = "/host/var/lib/dbus"
          }

          volume_mount {
            name       = "lib-modules"
            mount_path = "/lib/modules"
          }

          volume_mount {
            name       = "xtables-lock"
            mount_path = "/run/xtables.lock"
          }
        }

        container {
          name    = "weave-npc"
          image   = "docker.io/weaveworks/weave-npc:2.6.0"

          env {
            name = "HOSTNAME"
            value_from {
              field_ref {
                api_version = "v1"
                field_path  = "spec.nodeName"
              }
            }
          }

          resources {
            requests {
              cpu = "10m"
            }
          }

          security_context {
            privileged = true
          }

          volume_mount {
            name       = "xtables-lock"
            mount_path = "/run/xtables.lock"
          }
        }

        volume {
          name = "weavedb"
          host_path {
            path = "/var/lib/weave"
          }
        }

        volume {
          name = "cni-bin"
          host_path {
            path = "/opt"
          }
        }

        volume {
          name = "cni-bin2"
          host_path {
            path = "/home"
          }
        }

        volume {
          name = "cni-conf"
          host_path {
            path = "/etc"
          }
        }

        volume {
          name = "dbus"
          host_path {
            path = "/var/lib/dbus"
          }
        }

        volume {
          name = "lib-modules"
          host_path {
            path = "/lib/modules"
          }
        }

        volume {
          name = "xtables-lock"
          host_path {
            path = "/run/xtables.lock"
          }
        }
      }
    }
  }
}

