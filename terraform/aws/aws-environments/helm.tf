#-- Service account for HELM -------------------------------------------------
resource "kubernetes_service_account" "tiller" {
  automount_service_account_token = true

  metadata {
    name      = "env-${terraform.workspace}-tiller"
    namespace = "kube-system"
  }
}

resource "kubernetes_cluster_role_binding" "tiller" {
  metadata {
    name = kubernetes_service_account.tiller.metadata.0.name
  }

  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.tiller.metadata.0.name
    namespace = "kube-system"
  }

  # Temp to deal with a Terraform HELM provider not using the provided
  # service account...
  subject {
    kind      = "ServiceAccount"
    name      = "default"
    namespace = "kube-system"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }
}

#-- Node autoscaler ----------------------------------------------------------
resource "helm_release" "cluster_autoscaler" {
  depends_on = [kubernetes_cluster_role_binding.tiller, kubernetes_service_account.tiller]
  name       = "aws-cluster-autoscaler"
  namespace  = "kube-system"
  chart      = "stable/cluster-autoscaler"
  version    = "3.0.0"

  set {
    name  = "rbac.create"
    value = true
  }

  set_string {
    name = "dnsPolicy"
    value = "Default"
  }

  set_string {
    name  = "autoDiscovery.clusterName"
    value = "env-${terraform.workspace}-cluster"
  }

  set_string {
    name  = "awsRegion"
    value = var.aws_region
  }

  set_string {
    name  = "extraArgs.expander"
    value = "least-waste"
  }

  set_string {
    name  = "sslCertPath"
    value = "/etc/ssl/certs/ca-bundle.crt"
  }

  set {
    name  = "extraArgs.scale-down-enabled"
    value = true
  }

  set {
    name  = "extraArgs.balance-similar-node-groups"
    value = true
  }
}

resource "helm_release" "tiller_pdb" {
  depends_on = [kubernetes_cluster_role_binding.tiller, kubernetes_service_account.tiller]
  name       = "tiller-pdb"
  namespace  = "kube-system"
  chart      = "${path.module}/files/tiller-pdb-1.0.0.tgz"
  version    = "1.0.0"
}

# Metrics-server
resource "helm_release" "metrics-server" {
    name      = "metrics-server"
    chart     = "stable/metrics-server"
    namespace = "kube-system"

    set {
        name = "rbac.create"
        value = true
    }

    set {
        name = "serviceAccount.create"
        value = true
    }

    set {
        name = "serviceAccount.name"
        value = "metrics-server"
    }

    set {
        name  = "apiService.create"
        value = true
    }

    set {
        name = "hostNetwork.enabled"
        value = true
    }

    set_string {
        name = "service.labels.kubernetes\\.io/cluster-service"
        value = "true"
    }

    set_string {
        name = "service.labels.kubernetes\\.io/name"
        value = "Metrics-server"
    }

    set {
        name = "podDisruptionBudget.enabled"
        value = false
    }
}


# Kube-state-metrics
resource "helm_release" "kube-state-metrics" {
    name      = "kube-state-metrics"
    chart     = "stable/kube-state-metrics"
    namespace = "kube-system"

    set {
        name = "rbac.create"
        value = true
    }

    set {
        name = "serviceAccount.create"
        value = true
    }

    set {
        name = "serviceAccount.name"
        value = "kube-state-metrics"
    }

    set {
        name = "prometheusScrape"
        value = true
    }
}

# External DNS
resource "helm_release" "external-dns" {
    depends_on = [kubernetes_cluster_role_binding.tiller, kubernetes_service_account.tiller]
    name      = "external-dns"
    chart     = "stable/external-dns"
    namespace = "kube-system"

    set {
        name  = "sources"
        value = "{service,ingress}"
    }

    set {
        name = "provider"
        value = "aws"
    }

    set {
        name = "domainFilters"
        value = "{${aws_route53_zone.env_padok_co.name}}"
    }

    set {
        name = "policy"
        value = "sync"
    }

    set {
        name = "registry"
        value = "txt"
    }

    set {
        name = "txtOwnerId"
        value = aws_route53_zone.env_padok_co.zone_id
    }

    set {
        name = "txtPrefix"
        value = "env-${terraform.workspace}-cluster"
    }

#Need to setup this because default is 1min and Route53 has a high rate limiting on this API
    set {
        name = "interval"
        value = "5m"
    }

    set {
        name = "rbac.create"
        value = true
    }

    set {
        name = "rbac.serviceAccountName"
        value = "external-dns"
    }
}

# Ingress controller
resource "helm_release" "nginx-ingress-controller" {
    name      = "nginx-ingress-controller"
    chart     = "stable/nginx-ingress"
    namespace = "kube-system"

    set {
        name = "controller.autoscaling.enabled"
        value = true
    }

    set {
        name = "controller.autoscaling.minReplicas"
        value = 1
    }

    set {
        name = "controller.autoscaling.maxReplicas"
        value = aws_autoscaling_group.env_cluster_nodes.desired_capacity
    }

    set {
        name = "controller.metrics.enabled"
        value = true
    }

    set_string {
        name = "controller.config.use-forwarder-headers"
        value = true
    }

    set_string {
        name = "controller.config.compute-full-forwarded-for"
        value = true
    }

    set_string {
        name = "controller.config.use-proxy-protocol"
        value = true
    }

    set {
        name = "rbac.create"
        value = true
    }

    set {
        name = "serviceAccount.create"
        value = true
    }

    set {
        name = "controller.publishService.enabled"
        value = true
    }

    set_string {
        name = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-additional-resource-tags"
        value = "Name=ENV ${title(terraform.workspace)} Public zone-${var.aws_region}\\,Environment=${title(terraform.workspace)}\\,kubernetes.io/cluster/env-${terraform.workspace}-cluster=shared"
    }

    set_string {
        name = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-backend-protocol"
        value = "tcp"
    }

    set_string {
        name = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-connection-idle-timeout"
        value = "3600"
    }

    set {
        name = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-cross-zone-load-balancing-enabled"
        value = true
    }

    set_string {
        name = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-proxy-protocol"
        value = "*"
    }

    set_string {
        name = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-ssl-cert"
        value = aws_acm_certificate.wildcard_env_padok_co.arn
    }

    set_string {
        name = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-ssl-ports"
        value = "https"
    }

    set_string {
        name = "controller.service.targetPorts.https"
        value = "http"
    }
}

# external secret

data "helm_repository" "external-secrets" {
  name = "external-secrets"
  url  = "https://godaddy.github.io/kubernetes-external-secrets/"
}

resource "helm_release" "external-secrets" {
    name      = "external-secrets"
    chart     = "external-secrets/kubernetes-external-secrets"
    repository = data.helm_repository.external-secrets.metadata[0].name
    namespace = "kube-system"

    set_string {
        name = "env.AWS_REGION"
        value = var.aws_region
    }

    set_string {
        name = "env.POLLER_INTERVAL_MILLISECONDS"
        value = "86400000"
    }
}

#-- FluentD ---------------------------------------------------------------------------
resource "aws_iam_access_key" "fluentd" {
  user    = "fluentd"
}

data "helm_repository" "incubator" {
  name = "incubator"
  url  = "https://kubernetes-charts-incubator.storage.googleapis.com"
}

resource "helm_release" "fluentd" {
    name      = "fluentd"
    repository = data.helm_repository.incubator.metadata[0].name
    chart     = "incubator/fluentd-cloudwatch"
    namespace = "kube-system"

    set_string {
        name = "awsRegion"
        value = var.aws_region
    }

    set_string {
        name = "awsAccessKeyId"
        value = aws_iam_access_key.fluentd.id
    }

    set_string {
        name = "awsSecretAccessKey"
        value = aws_iam_access_key.fluentd.secret
    }

    set_string {
        name = "logGroupName"
        value = "/aws/eks/env-${terraform.workspace}-logs"
    }

    set_string {
        name = "rbac.create"
        value = true
    }
}
