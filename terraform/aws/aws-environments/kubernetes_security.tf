#-- Cluster Security groups --------------------------------------------------
resource "aws_security_group" "env_cluster" {
  name        = "env-${terraform.workspace}-cluster-sg"
  description = "Cluster communication"
  vpc_id      = aws_vpc.env_vpc.id
  tags        = merge(
                      var.resource_tags,
                      map("Name", "ENV ${title(terraform.workspace)} Cluster SG"),
                      map("Environment", "${title(terraform.workspace)}"),
                     )

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Whitelist HTTPS communication to the Master from allowed IPs
#resource "aws_security_group_rule" "env_cluster_inbound_whitelist" {
#  cidr_blocks       = var.cluster_inbound_whitelist_ips
#  description       = "Whistlisted IPs that can communicate with the API server"
#  security_group_id = aws_security_group.env_cluster.id
#
#  from_port = 443
#  to_port   = 443
#  protocol  = "tcp"
#  type      = "ingress"
#}

resource "aws_security_group_rule" "env_cluster_inbound_nodes" {
  description              = "Allow nodes to communicate with the API server"
  security_group_id        = aws_security_group.env_cluster.id
  source_security_group_id = aws_security_group.env_cluster_nodes.id

  from_port = 443
  to_port   = 443
  protocol  = "tcp"
  type      = "ingress"
}

#-- Nodes Security Groups ----------------------------------------------------
resource "aws_security_group" "env_cluster_nodes" {
  name        = "env-${terraform.workspace}-nodes-sg"
  description = "Nodes communication"
  vpc_id      = aws_vpc.env_vpc.id
  tags        = merge(
                      var.resource_tags,
                      map("Name", "ENV ${title(terraform.workspace)} Nodes SG"),
                      map("Environment", "${title(terraform.workspace)}"),
                      map("kubernetes.io/cluster/env-${terraform.workspace}-cluster", "owned")
                     )

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group_rule" "env_cluster_nodes_ingress_self" {
  description              = "Allow node to node communication"
  security_group_id        = aws_security_group.env_cluster_nodes.id
  source_security_group_id = aws_security_group.env_cluster_nodes.id

  from_port = 0
  to_port   = 65535
  protocol  = "-1"
  type      = "ingress"
}

resource "aws_security_group_rule" "env_cluster_nodes_ingress_cluster" {
  description              = "Allow inboud connections from master to nodes"
  security_group_id        = aws_security_group.env_cluster_nodes.id
  source_security_group_id = aws_security_group.env_cluster.id

  from_port = 1025
  to_port   = 65535
  protocol  = "tcp"
  type      = "ingress"
}

#-- Nodes SSH key ------------------------------------------------------------
resource "tls_private_key" "cluster_nodes_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "cluster_nodes_key" {
  key_name   = "ENV ${title(terraform.workspace)} cluster nodes key"
  public_key = tls_private_key.cluster_nodes_key.public_key_openssh
}
