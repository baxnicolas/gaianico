#-- AWS specific vars --------------------------------------------------------
variable "aws_region" {
  default = "us-east-1"
}

variable "aws_region_az" {
  type    = list
  default = ["a", "b"]
}

variable "resource_tags" {
  type    = map
  default = {
    "Terraform" = "True"
  }
}

variable "infra_account" {
    default = "676835720591"
}

variable "accounts" {
  type    = map
  default = {
    "app-staging" = "676835720591"
    "app-prod"    = "676835720591"
  }
}
